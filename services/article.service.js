import { fetchWrapper } from '../helpers/fetch-wrapper'

const url = typeof window === 'undefined' ? process.env.NEXT_PUBLIC_ROOT_URL
: window.location.origin

const baseUrl = `${url}/api/articles`;

function getArticles() {
    return fetchWrapper.get(`${baseUrl}`);
}

function getArticleById(id) {
    return fetchWrapper.get(`${baseUrl}/${id}`);
}

function createArticle(params) {
    return fetchWrapper.post(baseUrl, params);
}

function updateArticle(id, params) {
    return fetchWrapper.put(`${baseUrl}/${id}`, params);
}

function deleteArticle(id) {
    return fetchWrapper.delete(`${baseUrl}/${id}`);
}

export const articleService = {
    getArticles,
    getArticleById,
    createArticle,
    updateArticle,
    deleteArticle
};
