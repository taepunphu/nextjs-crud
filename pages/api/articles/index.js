var axios = require("axios");

const url = `${process.env.NEXT_PUBLIC_API_URL}/api/article`;

const getArticles = async (req, res) => {
  const resp = await axios({
    url: `${url}`,
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });

  const { status, data } = resp;

  if (status == 200) {
    res.status(200).json(data);
  } else {
    res.status(status).json(data);
  }
};

const createArticle = async (req, res) => {
  try {
    const { body } = req;

    var resp = await axios({
      url: `${url}`,
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      data: body,
    });

    const { data, status } = resp;

    res.status(status).json(data);
  } catch (ex) {
    console.log(ex);
    res.status(400).json(ex);
  }
};

export default async (req, res) => {
  const { method } = req;

  switch (method) {
    case "GET":
      return await getArticles(req, res);
    case "POST":
      return await createArticle(req, res);
    default:
      return res.status(405).end(`Method ${method} Not Allowed`);
  }
};