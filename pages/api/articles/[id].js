var axios = require("axios");

const url = `${process.env.NEXT_PUBLIC_API_URL}/api/article`;

const getArticleById = async (req, res) => {
  const { id } = req.query;

  const resp = await axios({
    url: `${url}/${id}`,
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });

  const { status, data } = resp;

  if (status == 200) {
    res.status(200).json(data);
  } else {
    res.status(status).json(data);
  }
};

const updateArticle = async (req, res) => {
  try {
    const { body } = req;

    const resp = await axios({
      url: `${url}/${body.id}`,
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      data: body,
    });

    const { status, data } = resp;

    res.status(status).json(data);
  } catch (ex) {
    console.log(ex);
    res.status(200).json({ success: true });
  }
};

const deleteArticle = async (req, res) => {
  const { id } = req.query;

  const resp = await axios({
    url: `${url}/${id}`,
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
  });

  const { status, data } = resp;

  if (status == 200) {
    res.status(200).json(data);
  } else {
    console.log(ex);
    res.status(status).json(data);
  }
};

export default async (req, res) => {
  const { method } = req;

  switch (method) {
    case "GET":
      return await getArticleById(req, res);
    case "PUT":
      return await updateArticle(req, res);
    case "DELETE":
      return await deleteArticle(req, res);
    default:
      return res.status(405).end(`Method ${method} Not Allowed`);
  }
};
