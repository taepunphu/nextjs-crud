import { Link } from "../components";

const Home = () => {
  return (
    <div>
      <h1>Next.js CRUD Example </h1>
      <p>
        <Link href="/articles">&gt;&gt; Manage Articles</Link>
      </p>
    </div>
  );
};

export default Home;
