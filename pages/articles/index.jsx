import { useState, useEffect } from "react";

import { Link } from "../../components";
import { articleService } from "../../services";

const Index = () => {
  const [articles, setArticles] = useState(null);

  useEffect(async () => {
    await articleService.getArticles().then((response) => {
      setArticles(response.results ?? []);
    });
  }, []);

  const deleteArticle = async (id) => {
   await articleService.deleteArticle(id).then((response) => {
      setArticles((articles) => articles.filter((x) => x.id !== id));
    });
  }

  return (
    <div>
      <h1>articles</h1>
      <Link href="/articles/add" className="btn btn-sm btn-success mb-2">
        Add Article
      </Link>
      <table className="table table-striped">
        <thead>
          <tr>
            <th style={{ width: "30%" }}>Title</th>
            <th style={{ width: "30%" }}>Content</th>
            <th style={{ width: "10%" }}></th>
          </tr>
        </thead>
        <tbody>
          {articles &&
            articles.map((article) => (
              <tr key={article.id}>
                <td>{article.title}</td>
                <td>{article.content}</td>
                <td style={{ whiteSpace: "nowrap" }}>
                  <Link
                    href={`/articles/edit/${article.id}`}
                    className="btn btn-sm btn-primary mr-1"
                  >
                    Edit
                  </Link>
                  <button
                    onClick={() => deleteArticle(article.id)}
                    className="btn btn-sm btn-danger btn-delete-user"
                  >
                    <span>Delete</span>
                  </button>
                </td>
              </tr>
            ))}
          {!articles && (
            <tr>
              <td colSpan="4" className="text-center">
                <div className="spinner-border spinner-border-lg align-center"></div>
              </td>
            </tr>
          )}
          {articles && !articles.length && (
            <tr>
              <td colSpan="4" className="text-center">
                <div className="p-2">No articles To Display</div>
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
};

export default Index;
