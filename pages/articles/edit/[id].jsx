import { AddEdit } from "../../../components/articles";
import { articleService } from "../../../services";

export default AddEdit;

export async function getServerSideProps({ params }) {
  const article = await articleService.getArticleById(params.id);

  return {
    props: { article },
  };
}
