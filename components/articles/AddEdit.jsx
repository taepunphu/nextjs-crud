import { useRouter } from "next/router";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";

import { Link } from "../../components";
import { articleService, alertService } from "../../services";

const AddEdit = (props) => {
  const article = props?.article;
  const isAddMode = !article;
  const router = useRouter();

  const validationSchema = Yup.object().shape({
    title: Yup.string().required("Title is required"),
    content: Yup.string().required("Content is required"),
  });

  const formOptions = { resolver: yupResolver(validationSchema) };

  if (!isAddMode) {
    const { ...defaultValues } = article.result;
    formOptions.defaultValues = defaultValues;
  }

  const { register, handleSubmit, reset, formState } = useForm(formOptions);
  const { errors } = formState;

  const onSubmit = (data) => {
    return isAddMode ? createArticle(data) : updateArticle(article.result.id, data);
  };

  const createArticle = (data) => {
    return articleService
      .createArticle(data)
      .then(() => {
        alertService.success("Article added", { keepAfterRouteChange: true });
        router.push("..");
      })
      .catch(alertService.error);
  };

  const updateArticle = (id, data) => {
    return articleService
      .updateArticle(id, data)
      .then(() => {
        alertService.success("Article updated", { keepAfterRouteChange: true });
        router.push("..");
      })
      .catch(alertService.error);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <h1>{isAddMode ? "Add Article" : "Edit Article"}</h1>
      <div className="form-row">
        <div className="form-group col-5">
          <label>Title</label>
          <input
            name="title"
            type="text"
            {...register("title")}
            className={`form-control ${errors.firstName ? "is-invalid" : ""}`}
          />
          <div className="invalid-feedback">{errors.firstName?.message}</div>
        </div>
        <div className="form-group col-5">
          <label>Content</label>
          <input
            name="content"
            type="text"
            {...register("content")}
            className={`form-control ${errors.lastName ? "is-invalid" : ""}`}
          />
          <div className="invalid-feedback">{errors.lastName?.message}</div>
        </div>
      </div>

      <div className="form-group">
        <button
          type="submit"
          disabled={formState.isSubmitting}
          className="btn btn-primary mr-2"
        >
          {formState.isSubmitting && (
            <span className="spinner-border spinner-border-sm mr-1"></span>
          )}
          Save
        </button>
        <button
          onClick={() => reset(formOptions.defaultValues)}
          type="button"
          disabled={formState.isSubmitting}
          className="btn btn-secondary"
        >
          Reset
        </button>
      </div>
    </form>
  );
};

export { AddEdit };
