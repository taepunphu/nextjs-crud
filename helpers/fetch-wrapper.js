import axios from 'axios'

function get(url, params) {
    return axios({
        url: url, 
        method: 'GET',
        params
    }).then(handleResponse)
}

function post(url, body, headers = {'Content-Type': 'application/json'}) {
    return axios({
        url,
        method: 'POST',
        headers,
        data: body
    })
    .then(handleResponse);
}

function put(url, body, headers = {'Content-Type': 'application/json'}) {
    return axios({
        url,
        method: 'PUT',
        headers,
        data: body
    })
    .then(handleResponse);
}

function _delete(url, params) {
    return axios({
        url,
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' },
        params
    })
    .then(handleResponse);
}

function handleResponse(response) {
    const {status, data} = response
    if(status === 200 || status === 204) {
        return data
    }else {
        throw new Error('error')
    }
}

export const fetchWrapper = {
    get,
    post,
    put,
    delete: _delete
};